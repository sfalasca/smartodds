#include "expr_forward.h"
#include "expr_simplify.h"
#include <array>
#include <math.h>

namespace Expr {
template <size_t Variables, size_t Literals>
struct EvaluationContext {
private:
  // Variables
  template <size_t i> friend struct Variable;

  template <size_t i>
  double variable() const {
    static_assert(i < Variables, "out of range access to variables");
    return variables[i];
  }

  template <size_t i>
  void variable(double v) {
    static_assert(i < Variables, "out of range access to variables");
    variables[i] = v;
  }

  std::array<double, Variables> variables;

  // Literals
  template <size_t i> friend struct Literal;

  template <size_t i>
  double literal() const {
    static_assert(i < Literals, "out of range access to literals");
    return literals[i];
  }
  
  template <size_t i>
  void literal(double v) {
    static_assert(i < Literals, "out of range access to literals");
    literals[i] = v;
  }

  std::array<double, Literals> literals;
};

template <typename E> struct Expr {};

struct Zero : Expr<Zero> {
  template <size_t i> using DType = Zero;

  template <size_t i> static DType<i> D(const Variable<i> &) { return {}; }

  template <size_t Variables, size_t Literals>
  static double eval(const EvaluationContext<Variables, Literals> &) { return 0.; }
};

struct One : Expr<One> {
  template <size_t i> using DType = Zero;

  template <size_t i> static DType<i> D(const Variable<i> &) { return {}; }

  template <size_t Variables, size_t Literals>
  static double eval(const EvaluationContext<Variables, Literals> &) { return 1.; }
};

template <size_t i, size_t j> struct VarDer { using Type = Zero; };
template <size_t i> struct VarDer<i, i> { using Type = One; };

template <size_t i> struct Variable : Expr<Variable<i>> {
  template <size_t Variables, size_t Literals>
  Variable(EvaluationContext<Variables, Literals> &ec, double v) { ec.template variable<i>(v); }

  template <size_t j> using DType = typename VarDer<i, j>::Type;

  template <size_t j> static DType<j> D(const Variable<j> &) { return {}; }

  template <size_t Variables, size_t Literals>
  static double eval(const EvaluationContext<Variables, Literals> &ec) { return ec.template variable<i>(); }
};

template <size_t i> struct Literal : Expr<Literal<i>> {
  template <size_t Variables, size_t Literals>
  Literal(EvaluationContext<Variables, Literals> &ec, double v) { ec.template literal<i>(v); }

  template <size_t j> using DType = Zero;

  template <size_t j> static DType<j> D(const Variable<j> &) { return {}; }

  template <size_t Variables, size_t Literals>
  static double eval(const EvaluationContext<Variables, Literals> &ec) { return ec.template literal<i>(); }
};

template <typename Operand, typename Operator>
struct UnaryOperation : Expr<UnaryOperation<Operand, Operator>> {
  template <size_t i>
  using DType = typename Operator::template DType<Operand, i>;

  template <size_t i> static DType<i> D(const Variable<i> &) { return {}; }

  template <size_t Variables, size_t Literals>
  static double eval(const EvaluationContext<Variables, Literals> &ec) {
    return Operator::eval(Operand::eval(ec));
  }
};

struct UMinus {
  template <typename Operand, size_t i>
  using DType = typename U<typename Operand::template DType<i>, UMinus>::type;

  static double eval(double v) { return -v; }
};

template <typename Operand>
typename U<Operand, UMinus>::type operator-(const Expr<Operand> &) {
  return {};
}

struct ULog {
  template <typename Operand, size_t i>
  using DType =
      typename B<typename Operand::template DType<i>, Operand, BDiv>::type;

  static double eval(double v) {
    if (v <= 0.) {
      throw 1;
    }
    return log(v);
  }
};

template <typename Operand>
typename U<Operand, ULog>::type Log(const Expr<Operand> &) {
  return {};
}

struct UExp {
  template <typename Operand, size_t i>
  using DType = typename B<typename U<Operand, UExp>::type,
                           typename Operand::template DType<i>, BTimes>::type;

  static double eval(double v) { return exp(v); }
};

template <typename Operand>
typename U<Operand, UExp>::type Exp(const Expr<Operand> &) {
  return {};
}

struct USqrt {
  template <typename Operand, size_t i>
  using DType =
      typename B<typename Operand::template DType<i>,
                 typename B<typename U<Operand, USqrt>::type,
                            typename U<Operand, USqrt>::type, BPlus>::type,
                 BDiv>::type;

  static double eval(double v) {
    if (v < 0.) {
      throw 1;
    }
    return sqrt(v);
  }
};

template <typename Operand>
typename U<Operand, USqrt>::type Sqrt(const Expr<Operand> &) {
  return {};
}

template <typename Lhs, typename Rhs, typename Operator>
struct BinaryOperation : Expr<BinaryOperation<Lhs, Rhs, Operator>> {
  template <size_t i>
  using DType = typename Operator::template DType<Lhs, Rhs, i>;

  template <size_t i> static DType<i> D(const Variable<i> &) { return {}; }

  template <size_t Variables, size_t Literals>
  static double eval(const EvaluationContext<Variables, Literals> &ec) {
    return Operator::eval(Lhs::eval(ec), Rhs::eval(ec));
  }
};

struct BPlus {
  template <typename Lhs, typename Rhs, size_t i>
  using DType = typename B<typename Lhs::template DType<i>,
                           typename Rhs::template DType<i>, BPlus>::type;

  static double eval(double lhs, double rhs) { return lhs + rhs; }
};

template <typename Lhs, typename Rhs>
typename B<Lhs, Rhs, BPlus>::type operator+(const Expr<Lhs> &,
                                            const Expr<Rhs> &) {
  return {};
}

struct BMinus {
  template <typename Lhs, typename Rhs, size_t i>
  using DType = typename B<typename Lhs::template DType<i>,
                           typename Rhs::template DType<i>, BMinus>::type;

  static double eval(double lhs, double rhs) { return lhs - rhs; }
};

template <typename Lhs, typename Rhs>
typename B<Lhs, Rhs, BMinus>::type operator-(const Expr<Lhs> &,
                                             const Expr<Rhs> &) {
  return {};
}

struct BTimes {
  template <typename Lhs, typename Rhs, size_t i>
  using DType =
      typename B<typename B<typename Lhs::template DType<i>, Rhs, BTimes>::type,
                 typename B<Lhs, typename Rhs::template DType<i>, BTimes>::type,
                 BPlus>::type;

  static double eval(double lhs, double rhs) { return lhs * rhs; }
};

template <typename Lhs, typename Rhs>
typename B<Lhs, Rhs, BTimes>::type operator*(const Expr<Lhs> &,
                                             const Expr<Rhs> &) {
  return {};
}

struct BDiv {
  template <typename Lhs, typename Rhs, size_t i>
  using DType = typename B<
      typename B<typename B<typename Lhs::template DType<i>, Rhs, BTimes>::type,
                 typename B<Lhs, typename Rhs::template DType<i>, BTimes>::type,
                 BMinus>::type,
      typename B<Rhs, Rhs, BTimes>::type, BDiv>::type;

  static double eval(double lhs, double rhs) { return lhs / rhs; }
};

template <typename Lhs, typename Rhs>
typename B<Lhs, Rhs, BDiv>::type operator/(const Expr<Lhs> &,
                                           const Expr<Rhs> &) {
  return {};
}
} // namespace Expr
