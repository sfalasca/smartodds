#pragma once

#include "expr_forward.h"

namespace Expr {
// Unary operations
template <typename Operand, typename Operator> struct U {
  using type = UnaryOperation<Operand, Operator>;
};

template <> struct U<Zero, UMinus> { using type = Zero; };

template <> struct U<One, ULog> { using type = Zero; };

template <typename E1, typename E2>
struct U<BinaryOperation<E1, E2, BTimes>, ULog> {
  using type = typename B<typename U<E1, ULog>::type,
                          typename U<E2, ULog>::type, BPlus>::type;
};

template <typename E1, typename E2>
struct U<BinaryOperation<E1, E2, BDiv>, ULog> {
  using type = typename B<typename U<E1, ULog>::type,
                          typename U<E2, ULog>::type, BMinus>::type;
};

template <typename E1> struct U<UnaryOperation<E1, UExp>, ULog> {
  using type = E1;
};

template <typename Operand> struct U<UnaryOperation<Operand, UMinus>, UMinus> {
  using type = Operand;
};

template <typename Lhs, typename Rhs>
struct U<BinaryOperation<Lhs, Rhs, BMinus>, UMinus> {
  using type = typename B<Rhs, Lhs, BMinus>::type;
};

template <typename E1, typename E2>
struct U<BinaryOperation<E1, E2, BTimes>, USqrt> {
  using type = typename B<typename U<E1, USqrt>::type,
                          typename U<E2, USqrt>::type, BTimes>::type;
};

template <typename E> struct U<BinaryOperation<E, E, BTimes>, USqrt> {
  using type = E;
};

// Binary operations
template <typename Lhs, typename Rhs, typename Operator> struct B {
  using type = BinaryOperation<Lhs, Rhs, Operator>;
};

template <typename E1, typename E2>
struct B<E2, BinaryOperation<E1, E2, BDiv>, BTimes> {
  using type = E1;
};

template <typename E1, typename E2>
struct B<BinaryOperation<E1, E2, BTimes>, E2, BDiv> {
  using type = E1;
};

template <typename E1, typename E2>
struct B<BinaryOperation<E1, E2, BTimes>, E1, BDiv> {
  using type = E2;
};

// Add
template <typename E> struct B<E, Zero, BPlus> { using type = E; };

template <typename E> struct B<Zero, E, BPlus> { using type = E; };

template <> struct B<Zero, Zero, BPlus> { using type = Zero; };

template <typename E> struct B<E, One, BTimes> { using type = E; };

template <typename E> struct B<One, E, BTimes> { using type = E; };

template <> struct B<One, One, BTimes> { using type = One; };

template <typename E> struct B<E, Zero, BTimes> { using type = Zero; };

template <typename E> struct B<Zero, E, BTimes> { using type = Zero; };

template <> struct B<One, Zero, BTimes> { using type = Zero; };

template <> struct B<Zero, One, BTimes> { using type = Zero; };

template <> struct B<Zero, Zero, BTimes> { using type = Zero; };

template <typename E> struct B<Zero, E, BDiv> { using type = Zero; };

template <typename E> struct B<E, One, BDiv> { using type = E; };

template <typename E> struct B<E, E, BDiv> { using type = One; };

template <typename E1, typename E2, typename E3>
struct B<BinaryOperation<E1, E2, BTimes>, E3, BDiv> {
  using type = typename B<typename B<E1, E3, BDiv>::type,
                          typename B<E2, E3, BDiv>::type, BTimes>::type;
};

template <> struct B<Zero, One, BDiv> { using type = Zero; };

template <typename E> struct B<One, BinaryOperation<One, E, BDiv>, BDiv> {
  using type = E;
};

template <typename E> struct B<Zero, UnaryOperation<E, UMinus>, BPlus> {
  using type = typename U<E, UMinus>::type;
};

template <typename E> struct B<Zero, E, BMinus> {
  using type = typename U<E, UMinus>::type;
};

template <typename E1> struct B<E1, Zero, BDiv>;

template <> struct B<One, Zero, BDiv>;

template <> struct B<Zero, Zero, BDiv>;
} // namespace Expr
