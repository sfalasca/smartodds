#pragma once

namespace Expr {
template <size_t Variables, size_t Literals>
struct EvaluationContext;

template <typename E> struct Expr;

// Variables
struct Zero;
struct One;

template <size_t i, size_t j> struct VarDer;
template <size_t i> struct Variable;

// Unary Operations
template <typename Operand, typename Operator> struct UnaryOperation;
template <typename Operand, typename Operator> struct U;

struct UMinus;
struct ULog;
struct UExp;
struct USqrt;

// Binary Operations
template <typename Lhs, typename Rhs, typename Operator> struct BinaryOperation;
template <typename Lhs, typename Rhs, typename Operator> struct B;

struct BPlus;
struct BMinus;
struct BTimes;
struct BDiv;
} // namespace Expr
