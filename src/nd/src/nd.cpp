#include "nd/nd.h"
#include "expr.h"

#include <assert.h>
#include <limits>
#include <math.h>
#include <tuple>

namespace {
auto log_pdf(double x_, double stddevValue, double meanValue) {
  using namespace Expr;
  auto ec = EvaluationContext<3, 1>{};
  auto x = Variable<0>{ec, x_};
  auto s = Variable<1>{ec, stddevValue};
  auto m = Variable<2>{ec, meanValue};
  auto piTimesTwo = Literal<0>{ec, 2. * M_PI};

  auto denominator = s * Sqrt(piTimesTwo);
  auto numerator = Exp(-((x - m) * (x - m)) / (s * s + s * s));
  auto expr = Log(numerator / denominator);

  return std::make_tuple(m, s, expr, ec);
}
}

NormalDistribution::Param::Param(const double mean, const double stddev)
    : mean(mean), stddev(stddev) {
  assert(stddev > 0.);
}

NormalDistribution::NormalDistribution(const double meanValue,
                                       const double stddevValue)
    : meanValue(meanValue), stddevValue(stddevValue) {
  assert(stddevValue > 0.);
}

NormalDistribution::NormalDistribution(const Param &param)
    : NormalDistribution{param.mean, param.stddev} {}

double NormalDistribution::mean() const { return meanValue; }

double NormalDistribution::stddev() const { return stddevValue; }

auto NormalDistribution::param() const -> Param {
  return {meanValue, stddevValue};
}

void NormalDistribution::param(const Param &param) {
  meanValue = param.mean;
  stddevValue = param.stddev;
}

double NormalDistribution::min() { return std::numeric_limits<double>::min(); }

double NormalDistribution::max() { return std::numeric_limits<double>::max(); }

double NormalDistribution::log_pdf(const double x) const {
  const auto &lpdf = ::log_pdf(x, stddevValue, meanValue);
  const auto &expr = std::get<2>(lpdf);
  const auto &ec = std::get<3>(lpdf);
  return expr.eval(ec);
}

std::array<double, 2>
NormalDistribution::gradient_log_pdf(const double x) const {
  const auto &lpdf = ::log_pdf(x, stddevValue, meanValue);
  const auto &m = std::get<0>(lpdf);
  const auto &s = std::get<1>(lpdf);
  const auto &expr = std::get<2>(lpdf);
  const auto &ec = std::get<3>(lpdf);

  return {expr.D(m).eval(ec), expr.D(s).eval(ec)};
}

std::array<std::array<double, 2>, 2>
NormalDistribution::hessian_log_pdf(const double x) const {
  const auto &lpdf = ::log_pdf(x, stddevValue, meanValue);
  const auto &m = std::get<0>(lpdf);
  const auto &s = std::get<1>(lpdf);
  const auto &expr = std::get<2>(lpdf);
  const auto &ec = std::get<3>(lpdf);

  const auto mm = expr.D(m).D(m).eval(ec);
  const auto ms = expr.D(m).D(s).eval(ec);
  const auto row1 = std::array<double, 2>{mm, ms};
  const auto sm = expr.D(s).D(m).eval(ec);
  const auto ss = expr.D(s).D(s).eval(ec);
  const auto row2 = std::array<double, 2>{sm, ss};
  return {row1, row2};
}

bool NormalDistribution::operator==(const NormalDistribution &rhs) const {
  return meanValue == rhs.meanValue && stddevValue == rhs.stddevValue;
}

bool NormalDistribution::operator!=(const NormalDistribution &rhs) const {
  return !(*this == rhs);
}
