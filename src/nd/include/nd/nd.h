#pragma once

#include <array>

struct NormalDistribution {
  /* A NormalDistribution object is characterised by two parameters being the
   * mean and the standard deviation. An object of type Param carries this
   * information, but it is meant to be used only to construct or specify the
   * parameters for a normal_distribution object, not to inspect the individual
   * parameters.
   */
  struct Param {
    double mean;
    double stddev;

  private:
    Param(double, double);
    friend NormalDistribution;
  };

  /*
   * The behaviour is undefined unless stddev is > 0.
   */
  NormalDistribution(double mean, double stddev);
  NormalDistribution(const Param &);

  double mean() const;
  double stddev() const;
  Param param() const;
  void param(const Param &);

  double static min();
  double static max();

  double log_pdf(double x) const;
  std::array<double, 2> gradient_log_pdf(double x) const;
  std::array<std::array<double, 2>, 2> hessian_log_pdf(double x) const;

  bool operator==(const NormalDistribution &rhs) const;
  bool operator!=(const NormalDistribution &rhs) const;

private:
  double meanValue;
  double stddevValue;
};
