#include "gtest/gtest.h"

#include "nd/nd.h"
#include <math.h>

TEST(NormalDistribution, Mean) {
  const auto nd = NormalDistribution{1., 2.};
  EXPECT_EQ(1., nd.mean());
}

TEST(NormalDistribution, StdDev) {
  const auto nd = NormalDistribution{1., 2.};
  EXPECT_EQ(2., nd.stddev());
}

TEST(NormalDistribution, GetParam) {
  const auto nd = NormalDistribution{1., 2.};
  const auto param = nd.param();
  EXPECT_EQ(1., param.mean);
  EXPECT_EQ(2., param.stddev);
}

TEST(NormalDistribution, ParamConstruction) {
  const auto nd = NormalDistribution{1., 2.};
  const auto nd2 = NormalDistribution{nd.param()};
  EXPECT_EQ(1., nd2.mean());
  EXPECT_EQ(2., nd2.stddev());
}

TEST(NormalDistribution, SetParam) {
  const auto nd = NormalDistribution{1., 2.};
  auto nd2 = NormalDistribution{3., 4.};
  nd2.param(nd.param());
  EXPECT_EQ(1., nd2.mean());
  EXPECT_EQ(2., nd2.stddev());
}

TEST(NormalDistribution, LogPdf) {
  const auto nd = NormalDistribution{1., 1.};
  EXPECT_DOUBLE_EQ(-(log(2 * M_PI) + 4) / 2., nd.log_pdf(-1.));
}

TEST(NormalDistribution, GradientLogPdf) {
  const auto nd = NormalDistribution{1., 2.};
  const auto gradient = nd.gradient_log_pdf(1.);
  EXPECT_DOUBLE_EQ(0., gradient[0]);
  EXPECT_DOUBLE_EQ((1. - 2. - 4. + 1.) / 8., gradient[1]);
}

TEST(NormalDistribution, HessianSymmetric) {
  {
    const auto nd = NormalDistribution{1., 2.};
    const auto hessian = nd.hessian_log_pdf(1.);
    EXPECT_DOUBLE_EQ(hessian[0][1], hessian[1][0]);
  }
  {
    const auto nd = NormalDistribution{5., 2.};
    const auto hessian = nd.hessian_log_pdf(2.);
    EXPECT_DOUBLE_EQ(hessian[0][1], hessian[1][0]);
  }
  {
    const auto nd = NormalDistribution{1., 2.};
    const auto hessian = nd.hessian_log_pdf(2.);
    EXPECT_DOUBLE_EQ(hessian[0][1], hessian[1][0]);
  }
  {
    const auto nd = NormalDistribution{21., 0.1};
    const auto hessian = nd.hessian_log_pdf(11.);
    EXPECT_DOUBLE_EQ(hessian[0][1], hessian[1][0]);
  }
  {
    const auto nd = NormalDistribution{1., 2.};
    const auto hessian = nd.hessian_log_pdf(1.);
    EXPECT_DOUBLE_EQ(hessian[0][1], hessian[1][0]);
  }
}

TEST(NormalDistribution, Hessian00) {
  {
    const auto nd = NormalDistribution{1., 2.};
    const auto hessian = nd.hessian_log_pdf(1.);
    EXPECT_DOUBLE_EQ(-1. / nd.stddev() / nd.stddev(), hessian[0][0]);
  }
  {
    const auto nd = NormalDistribution{5., 2.};
    const auto hessian = nd.hessian_log_pdf(2.);
    EXPECT_DOUBLE_EQ(-1. / nd.stddev() / nd.stddev(), hessian[0][0]);
    EXPECT_EQ(-1. / nd.stddev() / nd.stddev(), hessian[0][0]);
  }
  {
    const auto nd = NormalDistribution{1., 2.};
    const auto hessian = nd.hessian_log_pdf(2.);
    EXPECT_DOUBLE_EQ(-1. / nd.stddev() / nd.stddev(), hessian[0][0]);
  }
  {
    const auto nd = NormalDistribution{21., 0.1};
    const auto hessian = nd.hessian_log_pdf(11.);
    EXPECT_DOUBLE_EQ(-1. / nd.stddev() / nd.stddev(), hessian[0][0]);
  }
  {
    const auto nd = NormalDistribution{1., 2.};
    const auto hessian = nd.hessian_log_pdf(1.);
    EXPECT_DOUBLE_EQ(-1. / nd.stddev() / nd.stddev(), hessian[0][0]);
  }
}

TEST(NormalDistribution, Hessian01) {
  const auto nd = NormalDistribution{1., 2.};
  const auto hessian = nd.hessian_log_pdf(1.);
  EXPECT_DOUBLE_EQ(0., hessian[0][1]);
}

TEST(NormalDistribution, Hessian11) {
  const auto nd = NormalDistribution{1., 2.};
  const auto hessian = nd.hessian_log_pdf(1.);
  EXPECT_DOUBLE_EQ(1. / nd.stddev() / nd.stddev(), hessian[1][1]);
}

TEST(NormalDistribution, Equality) {
  const auto nd1 = NormalDistribution{1., 2.};
  const auto nd2 = NormalDistribution{1., 2.};
  EXPECT_TRUE(nd1 == nd2);
}

TEST(NormalDistribution, Inequality) {
  const auto nd1 = NormalDistribution{1., 2.};

  const auto nd2 = NormalDistribution{2., 2.};
  EXPECT_FALSE(nd1 == nd2);

  const auto nd3 = NormalDistribution{1., 3.};
  EXPECT_FALSE(nd1 == nd3);

  const auto nd4 = NormalDistribution{2., 3.};
  EXPECT_FALSE(nd1 == nd4);
}
